aptitude install lightdm
#/etc/lightdm/lightdm.conf 
cat <<EOT > /etc/lightdm/lightdm.conf 
[XDMCPServer]
enabled=true
MaxSessions=4
[LightDM]
start-default-seat=false
[SeatDefaults]
greeter-show-manual-login = true
greeter-hide-users = true
allow-guest = false
autologin-user=nobody
autologin-user-timeout=30
EOT

systemctl enable lightdm
systemctl start lightdm
systemctl restart lightdm
aptitude install tigervnc-standalone-server


cat <<EOT > /etc/systemd/system/xvnc@.service
[Unit]
Description=XVNC Per-Connection Daemon

[Service]
ExecStart=-/usr/bin/Xvnc -inetd -once -query localhost -geometry 1920x1080 -depth 24 -SecurityTypes=None
User=nobody
StandardInput=socket
StandardError=syslog
EOT

cat <<EOT > /etc/systemd/system/xvnc.socket
[Unit]
Description=XVNC Server

[Socket]
ListenStream=5900
Accept=yes

[Install]
WantedBy=sockets.target
EOT


systemctl enable xvnc.socket
systemctl start xvnc.socket


#cat <<EOT > /etc/polkit-1/localauthority/50-local.d/color.pkla
#[Allow colord for all users]
#Identity=unix-user:*
#Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile
#ResultAny=yes
#ResultInactive=yes
#ResultActive=yes
#EOT
