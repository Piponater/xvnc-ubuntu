#!/bin/bash


sudo yum install epel-release.noarch -y
sudo yum install xrdp xorgxrdp -y
sudo systemctl enable xrdp --now
#systemctl status xrdp
sudo sed -i 's/allow_channels=true/allow_channels=false/' /etc/xrdp/xrdp.ini
sudo sed -i '/^param=96.*/a param=-geometry \nparam=1920x1080 ' /etc/xrdp/sesman.ini 
sudo sed -i 's/max_bpp=32/max_bpp=24/g' /etc/xrdp/xrdp.ini
systemctl restart xrdp
systemctl status xrdp
#/usr/libexec/xrdp/startwm.sh >>> you edit the desktop session type from here
#chcon --type=bin_t /usr/sbin/xrdp
#chcon --type=bin_t /usr/sbin/xrdp-sesman
#systemctl restart xrdp
sudo firewall-cmd --add-port=3389/tcp --permanent
sudo firewall-cmd --reload
